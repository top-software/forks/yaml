module test_native

/**
 * Copyright 2021-2023 Camil Staps.
 * Copyright 2021 Gijs Alberts.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

import Data.Either
import Data.Error
import Data.Error.GenPrint
import Data.Functor
import Data.GenEq
from Data.Map import :: Map, instance == (Map k a)
import qualified Data.Map
import Data.Map.Gast
import Data.Map.GenPrint
import Data.Maybe
from Data.Set import :: Set, instance == (Set a)
import qualified Data.Set
import Data.Set.GenPrint
import Gast.CommandLine
import Gast.StdProperty
import Gast.Testable
import StdEnv
import Text

import Text.YAML
import Text.YAML.Construct

Start w = exposeProperties [] [] properties w
where
	properties =
		[ constructExample       as "Text.YAML.Construct: example from documentation"
		, constructMaybeExamples as "Text.YAML.Construct: examples with maybes"
		, constructTupleExamples as "Text.YAML.Construct: examples with tuples"
		, constructMapExamples   as "Text.YAML.Construct: examples with Maps"
		, constructSetExamples   as "Text.YAML.Construct: examples with Sets"
		]

derive genShow ErrorLocation, MaybeError, YAMLError, YAMLErrorWithLocations
derive gPrint Either, ErrorLocation, YAMLError, YAMLErrorWithLocations
instance == ErrorLocation derive gEq
instance == YAMLError derive gEq
instance == YAMLErrorWithLocations derive gEq

:: R1 = {int :: Int, adts :: [ADT1]}
:: ADT1 = NoArg | OneArg String | TwoArgs Char Real

derive genShow ADT1, R1
derive gPrint ADT1, R1
instance ConstructFromYAML ADT1 derive gConstructFromYAML
instance ConstructFromYAML R1 derive gConstructFromYAML
instance == ADT1 derive gEq
instance == R1 derive gEq

construct :== fmap fst o loadYAML coreSchema
testConstruct yaml node :== name yaml (construct yaml =.= Ok node)
testConstructFails yaml node :== (hd [construct yaml, Ok node])=:(Error _)

// Example from the documentation in Text.YAML.Construct
constructExample = construct yaml =.= Ok node
where
	node = {int = 5, adts = [NoArg, OneArg "one", TwoArgs 'x' 3.14]}

	yaml = join "\n"
		[ "int: 5"
		, "adts:"
		, "  - NoArg"
		, "  - OneArg: one"
		, "  - TwoArgs:"
		, "    - x"
		, "    - 3.14"
		]

:: R2 = {x :: ?Int, y :: ?Int}
:: R3 = {mm :: ?(?String)}

derive genShow R2, R3
derive gPrint R2, R3
instance ConstructFromYAML R2 derive gConstructFromYAML
instance ConstructFromYAML R3 derive gConstructFromYAML
instance == R2 derive gEq
instance == R3 derive gEq

constructMaybeExamples =
	testConstruct "null"          noneInt /\
	testConstruct "[]"            noneInt /\
	testConstruct "[1]"           (?Just 1) /\
	testConstruct "x: 1"          {x = ?Just 1, y = ?None} /\
	testConstruct "x: 1\ny: null" {x = ?Just 1, y = ?None} /\
	testConstruct "{}"            {mm = ?None} /\
	testConstruct "mm: [a]"       {mm = ?Just (?Just "a")} /\
	testConstruct "mm: null"      {mm = ?Just ?None} /\
	testConstruct "mm: [null]"    {mm = ?Just (?Just "null")}
where
	noneInt :: ?Int
	noneInt = ?None

constructTupleExamples =
	testConstruct      "[]"      () /\
	testConstruct      "[1,2,3]" (1,2,3) /\
	testConstructFails "[1,2]"   (1,2,3) /\
	testConstructFails "[1,2,3]" (1,2)

constructMapExamples =
	testConstruct      "{}"           emptyIntIntMap /\
	testConstructFails "{x: 1}"       emptyIntIntMap /\
	testConstruct      "{x: 1, y: 2}" ('Data.Map'.fromList [("x", 1), ("y", 2)]) /\
	testConstruct      "{2: y, 1: x}" ('Data.Map'.fromList [(1, "x"), (2, "y")])
where
	emptyIntIntMap :: Map Int Int
	emptyIntIntMap = 'Data.Map'.newMap

constructSetExamples =
	testConstruct      "[]"      emptyIntSet /\
	testConstruct      "[1,2,3]" ('Data.Set'.fromList [1,2,3]) /\
	testConstruct      "[1,a]"   ('Data.Set'.fromList ["1","a"])
where
	emptyIntSet :: Set Int
	emptyIntSet = 'Data.Set'.newSet
