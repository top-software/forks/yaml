implementation module Text.YAML

/**
 * Copyright 2021-2023 Camil Staps.
 * Copyright 2021 Gijs Alberts.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

import Control.Monad
import Data.Bifunctor
import Data.Either
import Data.Error
import Data.Functor
import Data.List
import Data.Tuple
import StdEnv

import Text.YAML.Compose
import Text.YAML.Construct
import Text.YAML.Parse
import Text.YAML.Schemas

instance toString YAMLError
where
	toString (IllFormed s) = "ill-formed: " +++ s
	toString (UnsupportedSyntax s) = "unsupported syntax: " +++ s
	toString (UnrecognizedTag s) = "unrecognized tag: " +++ s
	toString (UnidentifiedAlias s) = "unidentified alias: " +++ s
	toString UnresolvedTag = "unresolved tag"
	toString (InvalidContent s) = "invalid content: " +++ s

loadYAML :: !YAMLSchema !String -> MaybeError (Either YAMLError YAMLErrorWithLocations) (a, [String]) | ConstructFromYAML a
loadYAML schema yaml =
	loadStreamWithWarnings yaml >>= \(stream, warnings) -> case stream of
		[doc] -> bifmap Right (flip tuple warnings) (constructFromYAML schema (dynamic ()) doc)
		[] -> Error (Left (InvalidContent "stream contained no document"))
		_ -> Error (Left (InvalidContent "stream contained more than one document"))

loadMultipleYAML :: !YAMLSchema !String -> MaybeError (Either YAMLError YAMLErrorWithLocations) ([a], [String]) | ConstructFromYAML a
loadMultipleYAML schema yaml =
	loadStreamWithWarnings yaml >>= \(stream, warnings) ->
	bifmap Right (flip tuple warnings) (mapM (constructFromYAML schema (dynamic ())) stream)

loadStreamWithWarnings :: !String -> MaybeError (Either YAMLError e) ([YAMLNode], [String])
loadStreamWithWarnings yaml =
	mapError Left (parseYAMLStream yaml) >>= \events ->
	let
		(warnings`,events`) = partition (\e -> e=:(WarnEvent _)) events
		warnings = [w \\ WarnEvent w <- warnings`]
	in
	bifmap Left (flip tuple warnings) (composeYAMLGraphs events`)

withoutErrorLocations :: !YAMLError -> YAMLErrorWithLocations
withoutErrorLocations error = {error = error, locations = []}

pushErrorLocation :: !ErrorLocation !YAMLErrorWithLocations -> YAMLErrorWithLocations
pushErrorLocation loc error = {error & locations = [loc:error.locations]}
