definition module Text.YAML.ParseUtils

/**
 * This module is meant for internal use of the YAML library only.
 *
 * Copyright 2021 Camil Staps.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

from _SystemArray import class Array
from StdOverloaded import class ==

from Control.Applicative import class pure, class <*>, class <*, class Applicative
from Control.Monad import class Monad
from Data.Error import :: MaybeError
from Data.Functor import class Functor
from Data.Map import :: Map

from Text.YAML import :: YAMLError
from Text.YAML.Parse import :: YAMLEvent

:: ParseState arr t st =
	{ s         :: !arr t
	, idx       :: !Int
	, state     :: !st
	, forbidden :: !?(Parser arr t st ())
	}

:: Parser arr t st a =: Parser ((ParseState arr t st) -> ([(a, ParseState arr t st)], [YAMLError]))

:: StringParser st a :== Parser {#} Char st a

parse :: !(Parser arr t st a) !st !(arr t) -> MaybeError YAMLError (a, st) | Array arr t
	special arr={#}, t=Char

runParser :: !(Parser arr t st a) (ParseState arr t st) -> ([(a, ParseState arr t st)], [YAMLError])

instance Functor (Parser arr t st)

instance pure (Parser arr t st)
instance <*> (Parser arr t st)
instance <* (Parser arr t st)

instance Monad (Parser arr t st)

// NB: we do not define Alternative to force a choice between <?|> (which is
// like <|>) and <<|> (which does not use backtracking to save heap space).

pFail :: Parser arr t st a

pError :: !YAMLError -> Parser arr t st a
(@!) infixr 4 :: (Parser arr t st a) !YAMLError -> Parser arr t st a

pSatisfy :: !(t -> Bool) -> Parser arr t st t | Array arr t
	special arr={#}, t=Char
pToken :: t -> Parser arr t st t | Array arr t & == t
	special arr={#}, t=Char

pPeek :: Parser arr t st [t] | Array arr t
	special arr={#}, t=Char

/**
 * `<?|>` will always attempt the second parser if needed: if the path in which
 * the first parser was taken fails in a later step, it backtracks and tries
 * the second parser. `<<|>` will not backtrack; the second parser is only used
 * if the first parser fails immediately.
 */
(<?|>) infixr 4 :: (Parser arr t st a) (Parser arr t st a) -> Parser arr t st a

//* Choice parser without backtracking; see `<?|>`.
(<<|>) infixr 4 :: (Parser arr t st a) (Parser arr t st a) -> Parser arr t st a

//* Like `optional`, with backtracking (`<?|>`).
pOptional :: !(Parser arr t st a) -> Parser arr t st (?a)

//* Like `optional`, without backtracking (`<<|>`).
pFastOptional :: !(Parser arr t st a) -> Parser arr t st (?a)

(<:>) infixr 6 :: (Parser arr t st a) (Parser arr t st [a]) -> Parser arr t st [a]
pMany :: (Parser arr t st a) -> Parser arr t st [a]
pSome :: (Parser arr t st a) -> Parser arr t st [a]

pSepBy :: (Parser arr t st a) (Parser arr t st s) -> Parser arr t st [a]
pSepBy1 :: (Parser arr t st a) (Parser arr t st s) -> Parser arr t st [a]

//* Succeed when the input stream is empty.
pEof :: Parser arr t st [a] | Array arr t
	special arr={#}, t=Char

//* Rewind one token.
pRewind :: Parser arr t st ()

//* Fail if the argument parser matches. The input stream is not moved.
pReject :: !(Parser arr t st a) -> Parser arr t st ()

//* Succeed if the argument parser matches. The input stream is not moved.
pCheck :: !(Parser arr t st a) -> Parser arr t st ()

/**
 * Runs the left parser but fails if the right parser succeeds anywhere in the
 * consumed input.
 */
(`forbidding`) infix :: !(Parser arr t st a) !(Parser arr t st forbidden) -> Parser arr t st a

//* Succeed if the argument parser succeeds *and* progress the input index.
pNonEmpty :: !(Parser arr t st a) -> Parser arr t st a

//* Succeed at the start of a line.
pStartOfLine :: StringParser st ()
