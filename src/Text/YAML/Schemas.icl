implementation module Text.YAML.Schemas

/**
 * Copyright 2021 Camil Staps.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Data.Error
import Data.Func
import Data.Tuple
import Text

import Text.YAML
import Text.YAML.Compose

TAG_SEQ :: String
TAG_SEQ = "tag:yaml.org,2002:seq"

TAG_MAP :: String
TAG_MAP = "tag:yaml.org,2002:map"

TAG_STR :: String
TAG_STR = "tag:yaml.org,2002:str"

TAG_INT :: String
TAG_INT = "tag:yaml.org,2002:int"

TAG_BOOL :: String
TAG_BOOL = "tag:yaml.org,2002:bool"

TAG_FLOAT :: String
TAG_FLOAT = "tag:yaml.org,2002:float"

TAG_NULL :: String
TAG_NULL = "tag:yaml.org,2002:null"

resolveTag :: !YAMLSchema !YAMLNode -> MaybeError YAMLError String
resolveTag {tagResolver} {properties={tag}, content}
	| tag == "?" = case tagResolver content of
		?Just tag -> Ok tag
		?None -> Error UnresolvedTag
	| tag == "!" = case content of
		YAMLSequence _ -> Ok TAG_SEQ
		YAMLMapping _ -> Ok TAG_MAP
		YAMLScalar _ _ -> Ok TAG_STR
	| otherwise = Ok tag

failsafeSchema :: YAMLSchema
failsafeSchema =
	{ tagResolver = resolver
	, toBool      = abort "failsafe has no toBool\n"
	, toInt       = abort "failsafe has no toInt\n"
	, toReal      = abort "failsafe has no toReal\n"
	}
where
	resolver _ = ?None

jsonSchema :: YAMLSchema
jsonSchema =
	{ tagResolver = resolver
	, toBool      = parseBool
	, toInt       = toInt
	, toReal      = toReal
	}
where
	resolver (YAMLSequence _) = ?Just TAG_SEQ
	resolver (YAMLMapping _) = ?Just TAG_MAP
	resolver (YAMLScalar _ s)
		| s == "null"
			= ?Just TAG_NULL
		| s == "false" || s == "true"
			= ?Just TAG_BOOL
		| is_int cs
			= ?Just TAG_INT
		| is_real cs
			= ?Just TAG_FLOAT
			= ?None
	where
		cs = fromString s
		cs_tl = tl cs

		is_int ['-':cs] = is_int2 cs
		is_int cs = is_int2 cs
		is_int2 ['0'] = True
		is_int2 [c:cs] | isDigit c && c <> '0' = all isDigit cs
		is_int2 _ = False

		is_real ['-':cs] = is_real_int_part cs
		is_real cs = is_real_int_part cs
		is_real_int_part ['0':cs] = is_real_decimal_part cs
		is_real_int_part [c:cs] | isDigit c = is_real_decimal_part (dropWhile isDigit cs)
		is_real_int_part _ = False
		is_real_decimal_part ['.':cs] = is_real_exponent_part (dropWhile isDigit cs)
		is_real_decimal_part cs = is_real_exponent_part cs
		is_real_exponent_part ['e':cs] = is_real_exponent_part2 cs
		is_real_exponent_part ['E':cs] = is_real_exponent_part2 cs
		is_real_exponent_part [] = True
		is_real_exponent_part _ = False
		is_real_exponent_part2 ['+':cs=:[_:_]] = all isDigit cs
		is_real_exponent_part2 ['-':cs=:[_:_]] = all isDigit cs
		is_real_exponent_part2 [] = False
		is_real_exponent_part2 cs = all isDigit cs

	parseBool "true" = True
	parseBool "false" = False

coreSchema :: YAMLSchema
coreSchema =
	{ tagResolver = resolver
	, toBool      = parseBool
	, toInt       = parseInt
	, toReal      = parseReal
	}
where
	resolver (YAMLSequence _) = ?Just TAG_SEQ
	resolver (YAMLMapping _) = ?Just TAG_MAP
	resolver (YAMLScalar _ s) = scalarResolver s

	scalarResolver "null" = ?Just TAG_NULL
	scalarResolver "Null" = ?Just TAG_NULL
	scalarResolver "NULL" = ?Just TAG_NULL
	scalarResolver "~" = ?Just TAG_NULL
	scalarResolver "" = ?Just TAG_NULL
	scalarResolver "true" = ?Just TAG_BOOL
	scalarResolver "True" = ?Just TAG_BOOL
	scalarResolver "TRUE" = ?Just TAG_BOOL
	scalarResolver "false" = ?Just TAG_BOOL
	scalarResolver "False" = ?Just TAG_BOOL
	scalarResolver "FALSE" = ?Just TAG_BOOL
	scalarResolver s
		| (s.[0]=='-' || s.[0]=='+') && all isDigit cs_tl
			= ?Just TAG_INT
		| all isDigit cs
			= ?Just TAG_INT
		| s.[0]=='0' && size s > 2 && s.[1]=='o' && all (\c -> '0' <= c && c <= '7') cs_tl_tl
			= ?Just TAG_INT
		| s.[0]=='0' && size s > 2 && s.[1]=='x' && all isHexDigit cs_tl_tl
			= ?Just TAG_INT
		| (s.[0]=='-' || s.[0]=='+' || s.[0]=='.' || isDigit s.[0]) && is_real cs
			= ?Just TAG_FLOAT
		| (s.[0]=='-' || s.[0]=='+' || s.[0]=='.') &&
				isMember (toLowerCase (s % (1, size s - 1))) ["-.inf", "+.inf", ".inf"]
			= ?Just TAG_FLOAT
		| s.[0]=='.' && toLowerCase (s % (1, size s - 1)) == ".nan"
			= ?Just TAG_FLOAT
	where
		cs = fromString s
		cs_tl = tl cs
		cs_tl_tl = tl cs_tl
	scalarResolver _ = ?Just TAG_STR

	is_real ['-':cs] = is_real_decimals cs
	is_real ['+':cs] = is_real_decimals cs
	is_real ['.',c:cs] = isDigit c && is_real_scientific_notation (dropWhile isDigit cs)
	is_real cs = is_real_decimals cs
	is_real_decimals ['.',c:cs] = isDigit c && is_real_scientific_notation (dropWhile isDigit cs)
	is_real_decimals [c:cs] = isDigit c && is_real_dot (dropWhile isDigit cs)
	is_real_decimals _ = False
	is_real_dot ['.':cs] = is_real_scientific_notation (dropWhile isDigit cs)
	is_real_dot cs = is_real_scientific_notation cs
	is_real_scientific_notation ['e':cs] = is_real_exponent cs
	is_real_scientific_notation ['E':cs] = is_real_exponent cs
	is_real_scientific_notation [] = True
	is_real_scientific_notation _ = False
	is_real_exponent ['-':cs=:[_:_]] = all isDigit cs
	is_real_exponent ['+':cs=:[_:_]] = all isDigit cs
	is_real_exponent cs=:[_:_] = all isDigit cs
	is_real_exponent _ = False

	parseBool "true" = True
	parseBool "True" = True
	parseBool "TRUE" = True
	parseBool "false" = False
	parseBool "False" = False
	parseBool "FALSE" = False

	parseInt s
		| size s < 2
			= toInt s
		| s.[1] == 'o'
			= octalToInt 0 (tl (tl (fromString s)))
		| s.[1] == 'x'
			= hexToInt 0 (tl (tl (fromString s)))
			= toInt s
	where
		octalToInt i [] = i
		octalToInt i [c:cs] = (i << 3) + toInt (c - '0')

		hexToInt i [] = i
		hexToInt i [c:cs] = hexToInt ((i << 4) + hexDigitToInt c) cs
		where
			hexDigitToInt c
				| isDigit c
					= toInt (c - '0')
				| isUpper c
					= toInt (c - 'A') + 10
					= toInt (c - 'a') + 10

	parseReal s
		| last == 'f' || last == 'F'
			= if (s.[0] == '-') (~Infinity) Infinity
		| last == 'n' || last == 'N'
			= NaN
			= toReal $ toString $ adapt_real_notation $ fromString s
	where
		last = s.[size s-1]

	adapt_real_notation ['-':cs] = ['-':adapt_real_decimals cs]
	adapt_real_notation ['+':cs] = ['+':adapt_real_decimals cs]
	adapt_real_notation ['.':cs] = ['0.':adapt_real_scientific_notation cs]
	adapt_real_notation cs = adapt_real_decimals cs
	adapt_real_decimals ['.':cs] = ['.':digits ++ adapt_real_scientific_notation rest]
		where (digits,rest) = span isDigit cs
	adapt_real_decimals cs = digits ++ adapt_real_dot rest
		where (digits,rest) = span isDigit cs
	adapt_real_dot ['.':cs] = ['.':digits ++ adapt_real_scientific_notation rest]
		where (digits,rest) = span isDigit cs
	adapt_real_dot cs = digits ++ adapt_real_scientific_notation rest
		where (digits,rest) = span isDigit cs
	adapt_real_scientific_notation cs = cs
