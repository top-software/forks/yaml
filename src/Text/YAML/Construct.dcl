definition module Text.YAML.Construct

/**
 * This module provides the third step of reading YAML files: constructing a
 * native data structure from a node graph.
 *
 * This has the following behaviour:
 *
 * - Basic types are matched by appropriate scalar values.
 * - Records are YAML mappings with scalar keys matching the record's fields.
 * - When not all fields are specified, the fields that are not specified are
 *   assumed to have an empty scalar value `""` with the empty tag `""`. Since
 *   this tag does not occur in normal YAML nodes, you can recognize such nodes
 *   that are inserted by the `RECORD` instance by checking for the tag (this
 *   is what the `?` instance does to insert `?None` for unspecified fields).
 * - Constructors without arguments are simple scalar values.
 * - Constructors with one argument are mappings with one element; the key is
 *   the constructor name (as a scalar) and the value is the argument.
 * - Constructors with more than one argument are mappings with one element;
 *   the key is the contructor name (as a scalar) and the value is a sequence
 *   with its arguments.
 * - Lists and arrays are matched by sequences.
 *
 * Note that the rules for constructors are different from those defined for
 * JSON in Text.GenJSON.
 *
 * For example, the following Clean node:
 *
 * ```clean
 * :: R = {int :: Int, adts :: [ADT]}
 * :: ADT = NoArg | OneArg String | TwoArgs Char Real
 *
 * node = {int = 5, adts = [NoArg, OneArg "one", TwoArgs 'x' 3.14]}
 * ```
 *
 * would be parsed from this YAML:
 *
 * ```yaml
 * int: 5
 * adts:
 *   - NoArg
 *   - OneArg: one
 *   - TwoArgs:
 *     - x
 *     - 3.14
 * ```
 *
 * **Maybes**: record fields with a `?` type may be omitted, they are then
 * interpreted as `?None`. Otherwise, their value is interpreted as the
 * argument of `?Just`. Outside records, `null` and `[]` map to `?None` and
 * `[x]` maps to `?Just x`. The following examples show how `?` types are
 * handled:
 *
 * ```clean
 * R2 = {x :: ?Int, y :: ?Int}
 * R3 = {mm :: ?(?String)}
 *
 * "null"           ?None
 * "[]"             ?None
 * "[1]"            (?Just 1)
 * "x: 1"           {x = ?Just 1, y = ?None}
 * "x: 1\ny: null"  {x = ?Just 1, y = ?None}
 * "{}"             {mm = ?None}
 * "mm: [a]"        {mm = ?Just (?Just "a")}
 * "mm: null"       {mm = ?Just ?None}
 * "mm: [null]"     {mm = ?Just (?Just "null")}
 * ```
 *
 * Note that it is not possible to *explicitly* set the value of a `?(?a)`
 * field to `?None`: this is only possible by leaving the field unspecified.
 *
 * **Tuples and Void**: tuples are matched by sequences. `()` is matched by an
 * empty sequence.
 *
 * **Maps**: maps are matched by mappings, which do not need to be sorted in
 * the YAML representation but will be sorted in the Clean representation.
 * There needs to be an `==` and a `<` instance for the key type.
 *
 * **Sets**: sets are matched by sequences, which do not need to be sorted in
 * the YAML representation but will be sorted in the Clean representation.
 * There needs to be a `<` instance for the element type.
 *
 * Copyright 2021-2023 Camil Staps.
 * Copyright 2021 Gijs Alberts.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.Error import :: MaybeError
from Data.Map import :: Map
from Data.Set import :: Set
import StdGeneric
from StdOverloaded import class ==, class <
from _SystemStrictLists import class UList, class UTSList
from _SystemStrictMaybes import class UMaybe

from Text.YAML import :: YAMLError, :: YAMLErrorWithLocations
from Text.YAML.Compose import :: YAMLNode
from Text.YAML.Schemas import :: YAMLSchema

/**
 * Construct a native data structure from a YAML graph. For documentation on
 * adding support for custom types, see the `ConstructFromYAML` class.
 *
 * @param The schema to use.
 * @param A dynamic used to pass custom arguments to `ConstructFromYAML`
 *   instances. For details, see the `ConstructFromYAML` documentation.
 * @param The YAML graph to construct from.
 * @result A native value of the expected type, or an error.
 */
constructFromYAML :: !YAMLSchema !Dynamic !YAMLNode -> MaybeError YAMLErrorWithLocations a | ConstructFromYAML a

/**
 * An instance of this class is needed to construct Clean representations from
 * YAML graphs with `constructFromYAML`.
 *
 * Instances with default behavior (see the module documentation) can be
 * derived using `gConstructFromYAML`, for example:
 *
 * ```
 * instance ConstructFromYAML MyType derive gConstructFromYAML
 * ```
 *
 * When implementing a custom instance, be aware of the following:
 *
 * - There will be no `YAMLAlias` nodes; these are removed before constructing
 *   (see https://yaml.org/spec/1.2.2/#31-processes).
 * - The custom instance should consume the first `YAMLNode` of the stream, and
 *   return the tail of the list as the second element of the result tuple. The
 *   argument is a list to implement the `PAIR` instance.
 * - The instance should also check that there is no spurious extra data (e.g.
 *   uninterpretable fields in mappings).
 *
 * The dynamic argument is not used by any instances defined in clean-yaml
 * itself, and clean-yaml will use `()` as its value when calling
 * `ConstructFromYAML` internally. However, custom instances may use this to
 * pass extra information that can be used for constructing. For example, it
 * may contain a version number so that instances can check whether certain
 * syntax is allowed. For example, the following instance implements two ways
 * to construct values of type `T`, depending on a version number passed in the
 * dynamic argument:
 *
 * ```
 * instance ConstructFromYAML T
 * where
 *   ConstructFromYAML schema _ version nodes = case version of
 *     (version :: Int)
 *       | version >= 5 -> // .. implementation for versions >= 5
 *       | otherwise -> // .. implementation for versions < 5
 *     _
 *       -> abort "internal error in ConstructFromYAML T\n"
 * ```
 *
 * @param The schema to be used.
 * @param Whether we are parsing a record field.
 * @param A dynamic used to pass custom arguments. For details, see above.
 * @param The node stream (only one node should be consumed).
 * @result A constructed value with the rest of the stream, or an error.
 */
class ConstructFromYAML a :: !YAMLSchema !Bool !Dynamic ![YAMLNode] -> MaybeError YAMLErrorWithLocations (a, [YAMLNode])

//* For documentation, see the `ConstructFromYAML` class.
generic gConstructFromYAML a :: !YAMLSchema !Bool !Dynamic ![YAMLNode] -> MaybeError YAMLErrorWithLocations (a, [YAMLNode])
derive gConstructFromYAML UNIT, EITHER, PAIR
derive gConstructFromYAML OBJECT of {gtd_name}, CONS of {gcd_name,gcd_arity}
derive gConstructFromYAML RECORD of {grd_name,grd_fields}, FIELD of {gfd_name}

instance ConstructFromYAML String, Bool, Int, Real, Char

instance ConstructFromYAML
	[a] | ConstructFromYAML a, [!a] | ConstructFromYAML a, [a!] | ConstructFromYAML a, [!a!] | ConstructFromYAML a,
	[#a] | UList, ConstructFromYAML a, [#a!] | UTSList, ConstructFromYAML a,
	{a} | ConstructFromYAML a, {!a} | ConstructFromYAML a,
	(?a) | ConstructFromYAML a, (?^a) | ConstructFromYAML a, (?#a) | UMaybe, ConstructFromYAML a

instance ConstructFromYAML ()
instance ConstructFromYAML (a,b) | ConstructFromYAML a & ConstructFromYAML b
instance ConstructFromYAML (a,b,c) | ConstructFromYAML a & ConstructFromYAML b & ConstructFromYAML c
instance ConstructFromYAML (a,b,c,d) | ConstructFromYAML a & ConstructFromYAML b & ConstructFromYAML c & ConstructFromYAML d
instance ConstructFromYAML (a,b,c,d,e)
	| ConstructFromYAML a & ConstructFromYAML b & ConstructFromYAML c & ConstructFromYAML d
	& ConstructFromYAML e
instance ConstructFromYAML (a,b,c,d,e,f)
	| ConstructFromYAML a & ConstructFromYAML b & ConstructFromYAML c & ConstructFromYAML d
	& ConstructFromYAML e & ConstructFromYAML f
instance ConstructFromYAML (a,b,c,d,e,f,g)
	| ConstructFromYAML a & ConstructFromYAML b & ConstructFromYAML c & ConstructFromYAML d
	& ConstructFromYAML e & ConstructFromYAML f & ConstructFromYAML g
instance ConstructFromYAML (a,b,c,d,e,f,g,h)
	| ConstructFromYAML a & ConstructFromYAML b & ConstructFromYAML c & ConstructFromYAML d
	& ConstructFromYAML e & ConstructFromYAML f & ConstructFromYAML g & ConstructFromYAML h

instance ConstructFromYAML (Map k a) | ==, <, ConstructFromYAML k & ConstructFromYAML a
instance ConstructFromYAML (Set a) | <, ConstructFromYAML a

/**
 * Utility function to parse YAML scalars. This function can be used to define
 * a custom instance for `gConstructFromYAML`, for example:
 *
 * ```clean
 * gConstructFromYAML{|UInt|} schema in_record nodes =
 *     constructScalar [TAG_INT] parseUInt schema nodes
 * where
 *     parseUInt s = case toInt s of
 *         i | i >= 0 -> Ok i
 *         _ -> Error (InvalidContent "UInt must be non-negative")
 * ```
 *
 * @param The accepted tags (see Text.YAML.Schemas).
 * @param A parse function for the scalar content.
 * @param The schema (from the argument to `gConstructFromYAML`).
 * @param The nodes (from the argument to `gConstructFromYAML`).
 * @param The result for `gConstructFromYAML`.
 */
constructFromScalar :: ![String] !(String -> MaybeError YAMLError a) !YAMLSchema ![YAMLNode] -> MaybeError YAMLError (a, [YAMLNode])
