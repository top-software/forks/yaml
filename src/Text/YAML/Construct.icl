implementation module Text.YAML.Construct

/**
 * Copyright 2021-2023 Camil Staps.
 * Copyright 2021 Gijs Alberts.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

import Control.Monad
import Data.Bifunctor
import Data.Error
import Data.Func
import Data.Functor
import Data.List
from Data.Map import :: Map
import qualified Data.Map
import Data.Maybe
from Data.Set import :: Set
import qualified Data.Set
import Data.Tuple
import StdEnv
import StdGeneric
from Text import class Text(concat), instance Text String, concat3, concat4

import Text.YAML
import Text.YAML.Compose
import Text.YAML.Schemas

constructFromYAML :: !YAMLSchema !Dynamic !YAMLNode -> MaybeError YAMLErrorWithLocations a | ConstructFromYAML a
constructFromYAML schema dyn node =
	mapError withoutErrorLocations (removeAliases node []) >>= \(node,_) ->
	ConstructFromYAML schema False dyn [node] >>= \(x,rest) ->
	checkEmpty rest $> x

checkEmpty :: ![a] -> MaybeError YAMLErrorWithLocations ()
checkEmpty [] = Ok ()
checkEmpty _ = Error (withoutErrorLocations (InvalidContent "expected end of node list"))

// We cannot use a Map to keep the anchors; this would cause a cycle in spine.
// Unfortunately the type system won't allow us to keep references to parsed
// anchored nodes and retrieve them for aliases. (This is possible in
// Text.YAML.JSON, because the converted node is there always a JSONNode.)
// Therefore we remove aliases before entering gConstructFromYAML, and simply
// parse these nodes twice there.
removeAliases :: !YAMLNode [(String, YAMLNode)] -> MaybeError YAMLError (YAMLNode, [(String, YAMLNode)])
removeAliases node=:{properties={anchor = ?Just anchor}} anchors =
	let
		// NB: potential cycle in spine
		mbNodeAndAnchors = removeAliases
			{node & properties.anchor = ?None}
			[(anchor, node):anchors]
		(_,anchors) = fromOk mbNodeAndAnchors
	in
	mbNodeAndAnchors
removeAliases node=:{content=YAMLSequence xs} anchors =
	appFst (\xs -> {node & content=YAMLSequence xs}) <$> mapStM removeAliases xs anchors
removeAliases node=:{content=YAMLMapping kvs} anchors =
	appFst (\kvs -> {node & content=YAMLMapping kvs}) <$> mapStM removeKeyValueAliases kvs anchors
where
	removeKeyValueAliases {key,value} anchors =
		removeAliases key anchors >>= \(key, anchors) ->
		removeAliases value anchors >>= \(value, anchors) ->
		pure ({key=key, value=value}, anchors)
removeAliases node=:{content=YAMLScalar _ _} anchors =
	pure (node, anchors)
removeAliases {content=YAMLAlias alias} anchors = case lookup alias anchors of
	?Just node -> pure (node, anchors)
	?None -> Error (UnidentifiedAlias alias)

gConstructFromYAML{|UNIT|} _ _ dyn [] = pure (UNIT, [])

gConstructFromYAML{|EITHER|} fl fr schema in_record dyn nodes =
	case fl schema in_record dyn nodes of
		Ok (l, rest) -> Ok (LEFT l, rest)
		Error e -> case fr schema in_record dyn nodes of
			Ok (r, rest) -> Ok (RIGHT r, rest)
			_ -> Error e

gConstructFromYAML{|PAIR|} fx fy schema in_record dyn nodes =
	fx schema in_record dyn nodes >>= \(x,nodes) ->
	fy schema in_record dyn nodes >>= \(y,nodes) ->
	pure (PAIR x y, nodes)

gConstructFromYAML{|OBJECT of {gtd_name}|} fx schema in_record dyn [node:rest] =
	bifmap
		(pushErrorLocation (ADT gtd_name))
		(\(x, []) -> (OBJECT x, rest))
		(fx schema in_record dyn [node])
gConstructFromYAML{|OBJECT|} _ _ _ _ _ =
	Error (withoutErrorLocations (InvalidContent "unexpected EOF"))

gConstructFromYAML{|CONS of {gcd_name,gcd_arity=0}|} fx schema _ dyn nodes = case nodes of
	[{content=YAMLScalar _ s}:rest]
		| s == gcd_name ->
			bifmap
				(pushErrorLocation (Constructor gcd_name))
				(\(x, _) -> (CONS x, rest))
				(fx schema False dyn [])
		| otherwise ->
			Error
				{ error = InvalidContent "constructor name did not match"
				, locations = [Constructor s]
				}
	_ ->
		// Returning the constructor name does not provide valuable information in this case.
		Error $ withoutErrorLocations $
			InvalidContent "constructors without arguments are notated as scalars"
gConstructFromYAML{|CONS of {gcd_name,gcd_arity}|} fx schema _ dyn nodes = case nodes of
	[{content=YAMLMapping [{key={content=YAMLScalar _ s},value}]}:rest]
		| s == gcd_name
			| gcd_arity == 1 ->
				bifmap
					(pushErrorLocation (Constructor gcd_name))
					(\(x, _) -> (CONS x, rest))
					(fx schema False dyn [value])
			| otherwise -> case value.content of
				YAMLSequence args
					| length args <> gcd_arity ->
						Error $ withoutErrorLocations $
							InvalidContent $ concat4 gcd_name " constructor should have " (toString gcd_arity) " arguments"
					| otherwise ->
						bifmap
							(pushErrorLocation (Constructor gcd_name))
							(\(x, _) -> (CONS x, rest))
							(fx schema False dyn args)
				_ ->
					Error
						{ error = InvalidContent "a constructor with 2+ arguments is notated as a mapping of its name to a sequence of arguments"
						, locations = [Constructor gcd_name]
						}
		| otherwise ->
			Error
				{ error = InvalidContent "constructor name did not match"
				, locations = [Constructor s]
				}
	_ ->
		// Returning the constructor name does not provide valuable information in this case.
		Error $ withoutErrorLocations $
			InvalidContent "constructors are notated as mappings with a single entry, or scalars when they have no arguments"

// For records, we check the field keys in the RECORD instance. These should
// all be scalars and there should be no spurious keys. We also sort the values
// of the mapping by the order of the `grd_fields`, and insert empty scalars
// for fields that are not present. After this we can discard the information
// of keys and pass the list of values to the child generic.
gConstructFromYAML{|RECORD of {grd_name,grd_fields}|} fx schema _ dyn [{content=YAMLMapping kvs}:rest]
	| any (\{key} -> not (key.content=:(YAMLScalar _ _))) kvs =
		Error (withoutErrorLocations (InvalidContent ("complex key while parsing " +++ grd_name)))
	| not (isEmpty unknown_keys) =
		Error (withoutErrorLocations (InvalidContent (concat4 "unknown key " (hd unknown_keys) " while parsing " grd_name)))
	| otherwise =
		bifmap
			(\error=:{locations} -> case locations of
				[Field f:_] | not (isMember f keys) ->
					// Replace the error for fields for which no explicit value was given
					{error=InvalidContent (concat3 "required key " f " is not specified"), locations=[Record grd_name]}
				_ ->
					pushErrorLocation (Record grd_name) error)
			(\(x, []) -> (RECORD x, rest))
			(fx schema True dyn (map value grd_fields))
where
	all_keys = [s \\ {key={content=YAMLScalar _ s}} <- kvs]
	(keys,unknown_keys) = partition (flip isMember grd_fields) all_keys

	value f = case [value \\ {key={content=YAMLScalar _ s},value} <- kvs | s == f] of
		[] -> {properties = {tag = "", anchor = ?None}, content = YAMLScalar Plain ""}
		[n:_] -> n

gConstructFromYAML{|RECORD of {grd_name}|} _ _ _ _ _ =
	Error (withoutErrorLocations (InvalidContent ("expected mapping for " +++ grd_name)))

// See comments on the RECORD instance.
gConstructFromYAML{|FIELD of {gfd_name}|} fx schema True dyn nodes =
	bifmap (pushErrorLocation (Field gfd_name)) (appFst (\x -> FIELD x)) (fx schema True dyn nodes)

instance ConstructFromYAML String
where
	ConstructFromYAML schema _ _ nodes =
		mapError withoutErrorLocations $ constructFromScalar [TAG_STR, TAG_INT, TAG_BOOL, TAG_FLOAT, TAG_NULL] Ok schema nodes

instance ConstructFromYAML Bool
where
	ConstructFromYAML schema _ _ nodes =
		mapError withoutErrorLocations $ constructFromScalar [TAG_BOOL] (Ok o schema.toBool) schema nodes

instance ConstructFromYAML Int
where
	ConstructFromYAML schema _ _ nodes =
		mapError withoutErrorLocations $ constructFromScalar [TAG_INT] (Ok o schema.toInt) schema nodes

instance ConstructFromYAML Real
where
	ConstructFromYAML schema _ _ nodes =
		mapError withoutErrorLocations $ constructFromScalar [TAG_FLOAT, TAG_INT] (Ok o schema.toReal) schema nodes

instance ConstructFromYAML Char
where
	ConstructFromYAML schema _ _ nodes =
		mapError withoutErrorLocations $ constructFromScalar [TAG_STR, TAG_INT /* (for single digits) */] parse schema nodes
	where
		parse s = if (size s == 1) (Ok s.[0]) (Error (InvalidContent "too many characters for Char"))

constructFromScalar :: ![String] !(String -> MaybeError YAMLError a) !YAMLSchema ![YAMLNode] -> MaybeError YAMLError (a, [YAMLNode])
constructFromScalar allowed_tags parse schema nodes = case nodes of
	[node=:{content=YAMLScalar _ s}:rest] ->
		checkTag allowed_tags schema node >>|
		(\x -> (x, rest)) <$> parse s
	_ ->
		Error (InvalidContent (concat ["expected scalar of one of the types ":intersperse ", " allowed_tags]))

checkTag :: ![String] !YAMLSchema !YAMLNode -> MaybeError YAMLError String
checkTag allowed schema node =
	resolveTag schema node >>= \tag
		| isMember tag allowed
			-> Ok tag
			-> Error (InvalidContent (concat ["expected tag ",tag," to be one of ":intersperse ", " allowed]))

instance ConstructFromYAML [a] | ConstructFromYAML a
where
	ConstructFromYAML schema _ dyn nodes = case nodes of
		[node=:{content=YAMLSequence xs}:rest] ->
			(\xs -> (xs, rest)) <$> mapM constructOne [(i, x) \\ i <- [0..] & x <- xs]
		_ ->
			Error (withoutErrorLocations (InvalidContent "expected sequence for list"))
	where
		constructOne (i,node) = bifmap (pushErrorLocation (SequenceIndex i)) fst (ConstructFromYAML schema False dyn [node])

instance ConstructFromYAML [!a]  | ConstructFromYAML a where ConstructFromYAML schema _ dyn nodes = constructList schema dyn nodes
instance ConstructFromYAML [a!]  | ConstructFromYAML a where ConstructFromYAML schema _ dyn nodes = constructList schema dyn nodes
instance ConstructFromYAML [!a!] | ConstructFromYAML a where ConstructFromYAML schema _ dyn nodes = constructList schema dyn nodes
instance ConstructFromYAML [#a]  | UList, ConstructFromYAML a where ConstructFromYAML schema _ dyn nodes = constructList schema dyn nodes
instance ConstructFromYAML [#a!] | UTSList, ConstructFromYAML a where ConstructFromYAML schema _ dyn nodes = constructList schema dyn nodes

constructList schema dyn nodes :== appFst (\xs -> [|x \\ x <- xs]) <$> ConstructFromYAML schema False dyn nodes

instance ConstructFromYAML {a}  | ConstructFromYAML a where ConstructFromYAML schema _ dyn nodes = constructArray schema dyn nodes
instance ConstructFromYAML {!a} | ConstructFromYAML a where ConstructFromYAML schema _ dyn nodes = constructArray schema dyn nodes

constructArray schema dyn nodes :== appFst (\xs -> {x \\ x <- xs}) <$> ConstructFromYAML schema False dyn nodes

instance ConstructFromYAML (?a) | ConstructFromYAML a
where
	ConstructFromYAML schema False dyn nodes=:[{content=YAMLScalar _ _}:_] =
		mapError withoutErrorLocations $ constructFromScalar [TAG_NULL] (const (Ok ?None)) schema nodes
	ConstructFromYAML schema False _ nodes=:[{content=YAMLSequence []}:rest] =
		Ok (?None, rest)
	ConstructFromYAML schema False dyn nodes=:[{content=YAMLSequence arg=:[_]}:rest] =
		(\(x, []) -> (?Just x, rest)) <$> ConstructFromYAML schema False dyn arg
	ConstructFromYAML _ False _ _ =
		Error (withoutErrorLocations (IllFormed "Maybe types are matched by null or a sequence with less than two elements"))

	ConstructFromYAML schema True _ [{properties={tag=""},content=YAMLScalar Plain ""}:rest] =
		Ok (?None, rest)
	ConstructFromYAML schema True dyn nodes =
		case ConstructFromYAML schema False dyn nodes of
			Ok (x, rest) -> Ok (?Just x, rest)
			Error e -> mapError (const e) $ constructFromScalar [TAG_NULL] (const (Ok ?None)) schema nodes

instance ConstructFromYAML (?^a) | ConstructFromYAML a
where
	ConstructFromYAML schema in_record dyn nodes = appFst convertMaybe <$> ConstructFromYAML schema in_record dyn nodes

instance ConstructFromYAML (?#a) | UMaybe, ConstructFromYAML a
where
	ConstructFromYAML schema in_record dyn nodes = appFst convertMaybe <$> ConstructFromYAML schema in_record dyn nodes

convertMaybe m :== case m of
	?None -> ?|None
	?Just x -> ?|Just x

instance ConstructFromYAML ()
where
	ConstructFromYAML schema _ _ [{content=YAMLSequence []}:rest] =
		Ok ((), rest)
	ConstructFromYAML _ _ _ _ =
		Error (withoutErrorLocations (InvalidContent "expected empty sequence for ()"))

instance ConstructFromYAML (a,b) | ConstructFromYAML a & ConstructFromYAML b
where
	ConstructFromYAML schema _ dyn [{content=YAMLSequence xs}:rest] =
		ConstructFromYAML schema False dyn xs >>= \(a,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(b,xs) ->
		checkEmpty xs $> ((a, b), rest)
	ConstructFromYAML _ _ _ _ =
		Error (withoutErrorLocations (InvalidContent "tuples are matched by sequences"))

instance ConstructFromYAML (a,b,c) | ConstructFromYAML a & ConstructFromYAML b & ConstructFromYAML c
where
	ConstructFromYAML schema _ dyn [{content=YAMLSequence xs}:rest] =
		ConstructFromYAML schema False dyn xs >>= \(a,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(b,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(c,xs) ->
		checkEmpty xs $> ((a, b, c), rest)
	ConstructFromYAML _ _ _ _ =
		Error (withoutErrorLocations (InvalidContent "tuples are matched by sequences"))

instance ConstructFromYAML (a,b,c,d) | ConstructFromYAML a & ConstructFromYAML b & ConstructFromYAML c & ConstructFromYAML d
where
	ConstructFromYAML schema _ dyn [{content=YAMLSequence xs}:rest] =
		ConstructFromYAML schema False dyn xs >>= \(a,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(b,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(c,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(d,xs) ->
		checkEmpty xs $> ((a, b, c, d), rest)
	ConstructFromYAML _ _ _ _ =
		Error (withoutErrorLocations (InvalidContent "tuples are matched by sequences"))

instance ConstructFromYAML (a,b,c,d,e)
	| ConstructFromYAML a & ConstructFromYAML b & ConstructFromYAML c & ConstructFromYAML d
	& ConstructFromYAML e
where
	ConstructFromYAML schema _ dyn [{content=YAMLSequence xs}:rest] =
		ConstructFromYAML schema False dyn xs >>= \(a,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(b,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(c,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(d,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(e,xs) ->
		checkEmpty xs $> ((a, b, c, d, e), rest)
	ConstructFromYAML _ _ _ _ =
		Error (withoutErrorLocations (InvalidContent "tuples are matched by sequences"))

instance ConstructFromYAML (a,b,c,d,e,f)
	| ConstructFromYAML a & ConstructFromYAML b & ConstructFromYAML c & ConstructFromYAML d
	& ConstructFromYAML e & ConstructFromYAML f
where
	ConstructFromYAML schema _ dyn [{content=YAMLSequence xs}:rest] =
		ConstructFromYAML schema False dyn xs >>= \(a,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(b,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(c,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(d,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(e,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(f,xs) ->
		checkEmpty xs $> ((a, b, c, d, e, f), rest)
	ConstructFromYAML _ _ _ _ =
		Error (withoutErrorLocations (InvalidContent "tuples are matched by sequences"))

instance ConstructFromYAML (a,b,c,d,e,f,g)
	| ConstructFromYAML a & ConstructFromYAML b & ConstructFromYAML c & ConstructFromYAML d
	& ConstructFromYAML e & ConstructFromYAML f & ConstructFromYAML g
where
	ConstructFromYAML schema _ dyn [{content=YAMLSequence xs}:rest] =
		ConstructFromYAML schema False dyn xs >>= \(a,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(b,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(c,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(d,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(e,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(f,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(g,xs) ->
		checkEmpty xs $> ((a, b, c, d, e, f, g), rest)
	ConstructFromYAML _ _ _ _ =
		Error (withoutErrorLocations (InvalidContent "tuples are matched by sequences"))

instance ConstructFromYAML (a,b,c,d,e,f,g,h)
	| ConstructFromYAML a & ConstructFromYAML b & ConstructFromYAML c & ConstructFromYAML d
	& ConstructFromYAML e & ConstructFromYAML f & ConstructFromYAML g & ConstructFromYAML h
where
	ConstructFromYAML schema _ dyn [{content=YAMLSequence xs}:rest] =
		ConstructFromYAML schema False dyn xs >>= \(a,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(b,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(c,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(d,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(e,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(f,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(g,xs) ->
		ConstructFromYAML schema False dyn xs >>= \(h,xs) ->
		checkEmpty xs $> ((a, b, c, d, e, f, g, h), rest)
	ConstructFromYAML _ _ _ _ =
		Error (withoutErrorLocations (InvalidContent "tuples are matched by sequences"))

instance ConstructFromYAML (Map k a) | ==, <, ConstructFromYAML k & ConstructFromYAML a
where
	ConstructFromYAML schema _ dyn [{content=YAMLMapping kvs}:rest] =
		(\elems -> ('Data.Map'.fromList elems, rest)) <$>
		forM kvs
			(\{key,value} -> liftM2 tuple
				(fst <$> ConstructFromYAML schema True dyn [key])
				(fst <$> ConstructFromYAML schema True dyn [value]))

instance ConstructFromYAML (Set a) | <, ConstructFromYAML a
where
	ConstructFromYAML schema _ dyn nodes = appFst ('Data.Set'.fromList o isList) <$> ConstructFromYAML schema False dyn nodes
	where
		isList :: ![a] -> [a]
		isList xs = xs
