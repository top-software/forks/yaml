definition module Text.YAML.Schemas

/**
 * A YAML schema defines how the tags of scalar values without an explicit tag
 * should be resolved. See chapter 10 of the specification.
 *
 * By default, the `coreSchema` should be used.
 *
 * Copyright 2021 Camil Staps.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.Error import :: MaybeError

from Text.YAML import :: YAMLError
from Text.YAML.Compose import :: YAMLNode, :: YAMLNodeContent

:: YAMLSchema = !
	{ tagResolver :: !YAMLNodeContent -> ?String //* A resolver for nodes with a `?` non-specific tag
	, toBool      :: !String -> Bool //* To convert scalars that have been identified as bools to Bools
	, toInt       :: !String -> Int //* To convert scalars that have been identified as ints to Ints
	, toReal      :: !String -> Real //* To convert scalars that have been identified as floats to Reals
	}

TAG_SEQ :: String
TAG_MAP :: String
TAG_STR :: String
TAG_INT :: String
TAG_BOOL :: String
TAG_FLOAT :: String
TAG_NULL :: String

resolveTag :: !YAMLSchema !YAMLNode -> MaybeError YAMLError String

failsafeSchema :: YAMLSchema

jsonSchema :: YAMLSchema

coreSchema :: YAMLSchema
